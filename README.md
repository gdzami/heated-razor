# Install
/scripts/
npm install

# Development environment
- Choose your language in webpack.common.js : "const lng = 'fr'
- npm run start
http://localhost:8080 (FR version)
http://localhost:8080/index-{lng}.html
... create/update JS, SASS, HTML and Datas files in /src

# Production environment
- update absolute's path images (in css) : /src/sass/vars/path.scss
- npm run build 
... build JS, CSS and HTML files in /public

# Add Language

## 1- add file /src/datas/{lng}.json

## 2- update webpack.common.js
const languages = {
	fr: require(src + "datas/fr.json"),
	de: require(src + "datas/de.json")
    etc..
};

## 3- add medias
- /dist/videos/product-video/pub/gilette-heated-razor-{lng}.mp4
- /dist/videos/product-video/pub/gilette-heated-razor-mobile-{lng}.mp4
- /dist/img/footer/gillette_labs-{lng}.svg

## 4- LNG={lng} ENV={env} npm run build
- to generate only a locale version for the defined environment (fcinq or gillette)

## 5- ENV={env} npm run build-all
- to generate all versions for the defined environment (fcinq or gillette)
