import Header from "./partials/Header";
import Intro from "./sections/Intro";

import Config from "./Config";
import Senses from "./sections/Senses";
import Product from "./sections/Product";
import ProductWarmth from "./sections/ProductWarmth";
import ProductShaving from "./sections/ProductShaving";
import ProductExplosion from "./sections/ProductExplosion";
import Awards from "./sections/Awards";
import ProductVideo from "./sections/ProductVideo";

export default class Main {
  constructor() {
    this.scrolling = false;
    this.start = false;
    this.scrollTop = 0;

    if (!Config.s.touch) {
      document.documentElement.classList.add("no-touch");
    }

    this.detectSupport();
  }

  //-----------------------------------------------------o public
  update() {
    const st = Math.round(window.pageYOffset || document.documentElement.scrollTop);
    let direction = null;
    if (st > this.scrollTop) {
      direction = "bottom";
    } else if (st < this.scrollTop) {
      direction = "top";
    }

    if (this.start) {
      if (this.scrolling) {
        this.header.update(st);
        this.intro.update(st, direction);
        this.productVideoPub.update(st);
        this.senses.update(st);
        this.product.update(st);
        this.productWarmth.update(st, direction);
        this.productShaving.update(st, direction);
        this.productExplosion.update(st, direction);
        this.productVideoDemo.update(st);
        this.awards.update(st, direction);

        this.scrollTop = st;
        this.scrolling = false;
      } else {
        this.intro.update(st, direction);
        this.productExplosion.update(st, direction);
      }
    }
  }

  //-----------------------------------------------------o private
  init() {
    this.start = true;

    window.addEventListener("scroll", this.onScroll.bind(this));
    window.addEventListener("resize", this.onResize.bind(this));

    this.header = new Header();
    this.header.on(this.header.EVENT.SCROLL, options => {
      this.intro.setScrollHeader(options.status);
      this.senses.setScrollHeader(options.status);
      this.product.setScrollHeader(options.status);
      this.productWarmth.setScrollHeader(options.status);
      this.productShaving.setScrollHeader(options.status);
      this.productExplosion.setScrollHeader(options.status);
    });

    this.intro = new Intro();
    this.senses = new Senses();
    this.productVideoPub = new ProductVideo("product-video-pub");
    this.product = new Product();
    this.productWarmth = new ProductWarmth();
    this.productShaving = new ProductShaving();
    this.productExplosion = new ProductExplosion();
    this.productVideoDemo = new ProductVideo("product-video-demo");
    this.awards = new Awards();

    this.onResize();

    if (window.location.hash === "#shop") {
      setTimeout(() => {
        this.header.goto("shop");
      }, 100);
    }
  }

  detectSupport() {
    // -------------- Test support Passiven
    try {
      var opts = Object.defineProperty({}, "passive", {
        get: function() {
          Config.s.supportsPassive = true;
        }
      });
      window.addEventListener("testPassive", null, opts);
      window.removeEventListener("testPassive", null, opts);
    } catch (e) {}

    // -------------- Test support WEBP
    var supportsWebP = (function() {
      "use strict";
      return new Promise(function(A) {
        var n = new Image();
        (n.onerror = function() {
          return A(!1);
        }),
          (n.onload = function() {
            return A(1 === n.width);
          }),
          (n.src = "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=");
      }).catch(function() {
        return !1;
      });
    })();

    supportsWebP.then(supported => {
      if (supported) {
        Config.s.supportsWebp = true;
      }
      this.init();
    });
  }

  //-----------------------------------------------------o private handler
  onResize() {
    this.intro.resize();
    this.senses.resize();
    this.productVideoPub.resize();
    this.productWarmth.resize();
    this.productExplosion.resize();
    this.productVideoDemo.resize();
  }

  onScroll() {
    this.scrolling = true;
  }
}
