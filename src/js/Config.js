export default class Config {
  constructor() {}
}

Config.s = {
  supportsPassive: false,
  supportsWebp: false,
  isFirefox: navigator.userAgent.toLowerCase().indexOf("firefox") > -1,
  touch: "ontouchstart" in window || navigator.maxTouchPoints,
  mobile: navigator.userAgent.match(
    /Tablet|iPad|Mobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Opera Mini|\bCrMo\/|Opera Mobi/i
  ),
  dpr: window.devicePixelRatio !== undefined ? window.devicePixelRatio : 1,
  breakpoints: {
    xl: 1350,
    l: 1200,
    m: 1024,
    s: 900,
    xs: 640,
    xxs: 375
  },
  headerHeightMobile: 70
};
