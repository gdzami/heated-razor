import Video from "./_components/Video";
import { TweenMax, ScrollToPlugin } from "gsap/all";
import Config from "../Config";

const plugins = [ScrollToPlugin]; // because in "mode Production", bug without this line

export default class ProductVideo {
  constructor(id) {
    this.s = {};

    this.el = document.getElementById(id);
    this.video = null;
    this.initVideo();

    this.elBtnPlay = this.el.querySelector(".btn-play");
    this.initBtnPlay();
  }

  //-----------------------------------------------------o public
  resize() {
    this.video.resize();
  }

  update(st) {
    const wh = window.innerHeight;
    if (st > this.el.offsetTop + wh / 2 || st < this.el.offsetTop - wh / 2) {
      if (!this.video.isPaused()) {
        this.video.pause();
        this.elBtnPlay.classList.remove("hidden");
      }
      this.el.classList.remove("black");
      this.el.querySelector("video").classList.remove("front");
    }
  }

  //-----------------------------------------------------o private
  initVideo() {
    let file, resize, controls, ratio;
    if (Config.s.mobile && this.el.getAttribute("data-video-mobile")) {
      file = this.el.getAttribute("data-video-mobile");
      controls = true;
      ratio = this.el.getAttribute("data-ratio-mobile") === "portrait" ? 16 / 9 : 9 / 16;
      resize = "contains";
    } else {
      file = this.el.getAttribute("data-video");
      resize = "cover";
      controls = false;
      ratio = 9 / 16;
    }

    this.video = new Video(this.el, {
      preload: "none",
      ratio: ratio,
      loader: Config.s.mobile ? false : true,
      file: file,
      resize: resize,
      controls: controls,
      playsinline: true
    });

    this.video.on(this.video.EVENT.END, () => {});
  }

  initBtnPlay() {
    this.el.addEventListener("click", () => {
      if (this.video.isPaused()) {
        TweenMax.to(window, 0.3, { scrollTo: { y: this.el.offsetTop } });
        this.el.classList.add("black");
        this.el.querySelector("video").classList.add("front");
        this.video.play();
        this.elBtnPlay.classList.add("hidden");
        const arr = this.el.getAttribute("data-video").split("/");
        const label = arr[arr.length - 1];

        window.dataLayer.push({
          event: "HRButtonClick",
          eventCategory: "videos",
          eventAction: "play",
          eventLabel: label
        });
      } else {
        this.video.pause();
        this.elBtnPlay.classList.remove("hidden");
      }
    });
  }
}
