import { EventEmitter } from "events";

import { TweenMax, ScrollToPlugin } from "gsap/all";
const plugins = [ScrollToPlugin];

import Config from "../../Config";

export default class SequenceImages extends EventEmitter {
  constructor(el, options) {
    super(...arguments);

    this.EVENT = {
      START_AUTOPLAY: "startAutoplay"
    };

    this.s = {
      nbImgs: options.nbImgs,
      folderImg: options.folder,
      extension: options.extension,
      wheel: options.wheel,
      scroll: options.scroll,
      startImg: options.autoplay ? options.autoplay : 0,
      startLoop: options.startLoop ? options.startLoop : 0,
      ratio: options.ratio ? options.ratio : 810 / 1440,
      slow: options.slow ? options.slow : false,
      speedTimer: 30,
      pyEndTouch: 0,
      pyDist: 0
    };

    this.el = el;
    this.elSequence = this.el.querySelector(".sequence");
    this.canvas = this.elSequence.querySelector("canvas");
    this.context = this.canvas.getContext("2d");

    this.speedTimer = this.s.speedTimer;
    this.timerInterval = null;
    this.timerAnimation = null;
    this.scrollWheel = false;
    this.direction = null;
    this.index = 0;
    this.imgs = [];

    if (this.s.wheel) {
      this.initEvents();
    }

    this.addLoader();
    this.loadImg(1);
  }

  //-----------------------------------------------------o public
  resize() {
    const ww = this.el.clientWidth;
    const wh = this.el.clientHeight;
    const rw = wh / ww;
    const ri = this.s.ratio;
    let w, h, mLeft, mTop;

    if (rw > ri) {
      w = wh / ri;
      h = wh;
    } else {
      w = ww;
      h = ww * ri;
    }

    mLeft = ww / 2 - w / 2;
    mTop = wh / 2 - h / 2;

    this.elSequence.style.width = Math.ceil(w) + "px";
    this.elSequence.style.height = Math.ceil(h) + "px";
    this.elSequence.style.left = Math.ceil(mLeft) + "px";
    this.elSequence.style.top = Math.ceil(mTop) + "px";

    this.canvas.width = Math.ceil(w);
    this.canvas.height = Math.ceil(h);

    this.draw();
  }

  update(st, direction) {
    const wh = window.innerHeight;
    const t = this.el.offsetTop;

    // ----------------- scroll native
    if (this.s.scroll) {
      this.direction = direction;

      if ((direction == "bottom" && st > t - wh * 0.25 && st < t + wh) || (direction == "top" && st > t - wh && st < t + wh * 0.5)) {
        this.beforeAnimate();
      }
      // ----------------- scroll weel
    } else if (this.s.wheel) {
      if ((this.el.classList.contains("fixed") || st === t) && !this.scrollWheel) {
        if (this.index === this.s.nbImgs - 1 && this.direction == "bottom") {
          this.scrollWheel = false;
        } else {
          this.scrollWheel = true;
        }
      }

      if (this.scrollWheel && this.direction !== null) {
        this.beforeAnimate();
      } else if (this.s.startLoop > 0 && this.index < this.s.startImg && this.direction === null && this.timerInterval === null) {
        this.playAuto(this.s.startImg, "bottom", true);
      }

      // ----------------- mode auto
    } else if (this.timerInterval !== null) {
      this.beforeAnimate();
    }
  }

  play(index, speed) {
    const direction = index > this.index ? "bottom" : "top";
    if (speed) {
      this.playAuto(index, direction, false, speed);
    } else {
      this.playAuto(index, direction);
    }

    if (this.s.wheel) {
      TweenMax.to(window, 0.3, { scrollTo: { y: this.el.offsetTop } });
    }
  }

  getIndexImg() {
    return this.index;
  }

  getDirection() {
    return this.direction;
  }

  getModeAuto() {
    return this.timerInterval;
  }

  resetStart() {
    this.clearTimer();
    this.scrollWheel = false;
    this.direction = null;
    this.index = 0;
    this.draw();
  }

  resetEnd() {
    this.clearTimer();
    if (this.imgs.length === this.s.nbImgs) {
      this.scrollWheel = false;
      this.direction = null;
      this.index = this.s.nbImgs - 1;
      this.draw();
    }
  }

  //-----------------------------------------------------o private
  initEvents() {
    const wheelEvent =
      "onwheel" in document.createElement("div")
        ? "wheel" // Modern browsers support "wheel"
        : document.onmousewheel !== undefined
        ? "mousewheel" // Webkit and IE support at least "mousewheel"
        : "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

    this.el.addEventListener(wheelEvent, this.onMouseWheel.bind(this));

    this.elSequence.addEventListener("mousedown", this.onDown.bind(this));
    this.elSequence.addEventListener("touchstart", this.onDown.bind(this));
  }

  loadImg(i) {
    let img = new Image();
    img.onload = () => {
      this.imgs.push(img);
      if (i === this.s.nbImgs) {
        this.removeLoader();
      }
      if (i < this.s.nbImgs) {
        if (i === 1) {
          this.draw();
        }
        this.startAutoPlay(i);
        this.loadImg(i + 1);
      }
    };

    img.src = this.s.folderImg + i + this.s.extension;
  }

  startAutoPlay(i) {
    if (this.s.startImg > 0 && i === this.s.startImg) {
      const st = window.pageYOffset || document.documentElement.scrollTop;
      const t = this.el.offsetTop;
      if (st >= t && st < t + window.innerHeight) {
        this.playAuto(this.s.startImg, "bottom", true);
      }
    }
  }

  playAuto(index, direction, loop, duration) {
    const d = duration ? duration : 40;
    this.clearTimer();
    this.clearTimerAnimation();
    this.timerInterval = setInterval(() => {
      if (this.index === index) {
        this.clearTimer();
        if (this.s.startLoop && loop > 0) {
          this.loopAnimation();
        }
      }
      this.direction = direction;
    }, d);
  }

  loopAnimation() {
    if (this.index === this.s.startImg) {
      this.playAuto(this.s.startLoop, "top", true, 90);
    } else {
      this.playAuto(this.s.startImg, "bottom", true, 90);
    }
  }

  clearTimer() {
    if (this.timerInterval !== null) {
      clearInterval(this.timerInterval);
      this.timerInterval = null;
    }
  }

  clearTimerAnimation() {
    if (this.timerAnimation !== null) {
      clearTimeout(this.timerAnimation);
      this.timerAnimation = null;
    }
  }

  beforeAnimate() {
    if (this.direction === "bottom" || this.direction === "top") {
      this.animate();

      if (this.index === this.s.nbImgs - 1 || this.index === 0) {
        this.scrollWheel = false;
      }
    }
  }

  animate() {
    if (this.direction === "bottom" && this.index < this.s.nbImgs - 1) {
      if (this.index + 1 < this.imgs.length) {
        this.index += 1;
      }
    } else if (this.direction === "top" && this.index > this.s.startLoop) {
      this.index -= 1;
    }

    this.direction = null;
    this.clearTimerAnimation();

    if (this.s.slow && this.timerInterval === null) {
      this.timerAnimation = setTimeout(() => {
        this.draw();
        this.timerAnimation = null;
      }, this.speedTimer);
    } else {
      this.draw();
    }
  }

  draw() {
    if (this.index < this.imgs.length) {
      this.context.clearRect(0, 0, this.elSequence.clientWidth, this.elSequence.clientHeight);
      this.context.drawImage(this.imgs[this.index], 0, 0, this.elSequence.clientWidth, this.elSequence.clientHeight);
    }
  }

  addLoader() {
    if (!this.elSequence.classList.contains("loader")) {
      this.elSequence.classList.add("loader");
    }
  }

  removeLoader() {
    if (this.elSequence.classList.contains("loader")) {
      this.elSequence.classList.remove("loader");
    }
  }

  //-----------------------------------------------------o private handlers
  onMouseWheel(event) {
    if (this.scrollWheel && this.s.wheel) {
      this.clearTimer();
      this.speedTimer = this.s.speedTimer;

      event.preventDefault();
      event.stopPropagation();
      if (Math.abs(event.deltaY) > 0.75 && this.timerAnimation === null) {
        this.direction = event.deltaY > 0 ? "bottom" : "top";
      } else {
        this.direction = null;
      }
      return false;
    }
  }

  onDown(event) {
    this.s.pyDist = 0;
    this.s.pyEndTouch = 0;
    this.speedTimer = this.s.speedTimer;

    this.clearTimer();
    this.clearTimerAnimation();
    this.onMoveHandler = this.onMove.bind(this);
    this.onUpHandler = this.onUp.bind(this);

    this.elSequence.addEventListener("mousemove", this.onMoveHandler);
    this.elSequence.addEventListener("mouseup", this.onUpHandler, { passive: Config.s.supportsPassive });
    this.elSequence.addEventListener("mouseleave", this.onUpHandler, { passive: Config.s.supportsPassive });

    this.elSequence.addEventListener("touchmove", this.onMoveHandler);
    this.elSequence.addEventListener("touchend", this.onUpHandler, { passive: Config.s.supportsPassive });
  }

  onUp() {
    this.s.pyEndTouch = 0;
    this.elSequence.removeEventListener("mousemove", this.onMoveHandler);
    this.elSequence.removeEventListener("mouseup", this.onUpHandler);
    this.elSequence.removeEventListener("mouseleave", this.onUpHandler);

    this.elSequence.removeEventListener("touchmove", this.onMoveHandler);
    this.elSequence.removeEventListener("touchend", this.onUpHandler);

    if (this.s.pyDist > 3 && this.index < this.s.nbImgs - 1) {
      if (this.imgs.length === this.s.nbImgs) {
        this.play(this.s.nbImgs - 1, 30);
      } else {
        this.play(this.imgs.length - 1, 30);
      }

      this.emit(this.EVENT.START_AUTOPLAY);
    }
  }

  onMove(event) {
    if (this.scrollWheel && this.s.wheel) {
      if (event.cancelable) {
        event.preventDefault();
        event.stopPropagation();
      }
      const py = event.touches && event.touches.length ? event.touches[0].pageY : event.pageY;
      const dist = Math.abs(this.s.pyEndTouch - py);
      this.clearTimer();

      if (py > this.s.pyEndTouch && dist >= 2 && this.timerAnimation === null) {
        this.direction = "top";
      } else if (py < this.s.pyEndTouch && dist >= 2 && this.timerAnimation === null) {
        this.direction = "bottom";
      } else {
        this.direction = null;
      }
      this.s.pyDist = this.s.pyEndTouch - py;
      this.s.pyEndTouch = py;
    }
  }
}
