import { EventEmitter } from "events";
import Config from "../../Config";

export default class Video extends EventEmitter {
  constructor(el, options) {
    super(...arguments);

    this.EVENT = {
      CANPLAY: "canplay",
      END: "end"
    };

    this.el = el;
    this.options = options;

    this.metadataReady = false;
    this.canplay = false;
    this.playing = false;
    this.elVideo = null;
    this.loader = null;

    this.init();
  }

  //-----------------------------------------------------o private
  init() {
    let dom = "<video>";
    dom += '<source src="' + this.options.file + '" type="video/mp4">';
    dom += "</video>";

    this.el.innerHTML += dom;
    this.elVideo = this.el.querySelector("video");

    if (this.options.playsinline) this.elVideo.setAttribute("playsinline", true);

    if (this.options.autoplay) {
      this.elVideo.setAttribute("autoplay", true);
      if (this.options.loader && !this.options.controls) this.addBuffer();
      this.elVideo.classList.add("front");
    }

    if (this.options.preload) this.elVideo.setAttribute("preload", this.options.preload);
    if (this.options.poster && Config.s.mobile) this.elVideo.setAttribute("poster", this.options.poster);
    if (this.options.loop) this.elVideo.setAttribute("loop", true);
    if (this.options.muted) this.elVideo.setAttribute("muted", true);
    if (this.options.controls) this.elVideo.controls = true;

    this.canplaythrough = this._canplaythrough.bind(this);
    this.canplay = this._canplay.bind(this);
    this.videoMetadataReady = this._videoMetadataReady.bind(this);
    this.videoWaiting = this._videoWaiting.bind(this);
    this.videoPlaying = this._videoPlaying.bind(this);
    this.videoEnd = this._videoEnd.bind(this);

    this.elVideo.addEventListener("canplaythrough", this.canplaythrough);
    this.elVideo.addEventListener("canplay", this.canplay);
    this.elVideo.addEventListener("loadedmetadata", this.videoMetadataReady);
    this.elVideo.addEventListener("waiting", this.videoWaiting);
    this.elVideo.addEventListener("playing", this.videoPlaying);
    this.elVideo.addEventListener("ended", this.videoEnd);
  }

  //-----------------------------------------------------o private buffer
  addBuffer() {
    if (this.loader === null) {
      this.loader = document.createElement("span");
      this.loader.classList.add("icon-loader");
      this.el.appendChild(this.loader);
    }
  }

  removeBuffer() {
    if (this.loader !== null) {
      this.el.removeChild(this.loader);
      this.loader = null;
    }
  }

  //-----------------------------------------------------o private handlers
  _canplay(event) {
    this.options.duration = event.target.duration;

    this.elVideo.removeEventListener("canplaythrough", this.canplaythrough);
    this.elVideo.removeEventListener("canplay", this.canplay);

    this.emit(this.EVENT.CANPLAY);
    this.canplay = true;

    this.resize();
  }

  _canplaythrough(event) {
    this._canplay(event);
  }

  _videoMetadataReady(e) {
    this.metadataReady = true;
  }

  _videoWaiting(e) {
    if (this.options.loader) this.addBuffer();
  }

  _videoPlaying(e) {
    this.removeBuffer();
  }

  _videoEnd(e) {
    // this.elVideo.classList.remove('front');
    // this.elVideo.seek = 0;
    this.elVideo.classList.remove("front");

    this.emit(this.EVENT.END);
  }

  _videoTimeUpdate(e) {
    // let currentTime = e.target.currentTime;
  }

  //-----------------------------------------------------o public
  pause() {
    if (this.elVideo !== null) {
      this.elVideo.pause();
      this.playing = false;
      if (this.options.loader) this.removeBuffer();
    }
  }

  isPaused() {
    if (this.elVideo.paused) {
      return true;
    } else {
      return false;
    }
  }

  play(restart) {
    if (this.elVideo !== null) {
      if (restart) this.elVideo.currentTime = 0;

      try {
        this.elVideo.play();
        this.playing = true;
      } catch (error) {
        console.error(error);
      }

      this.elVideo.classList.add("front");
    }
  }

  rewind() {
    this.elVideo.currentTime = 0;
  }

  hide() {
    if (!this.elVideo.classList.contains("hide")) this.elVideo.classList.add("hide");
  }

  show() {
    if (this.elVideo.classList.contains("hide")) {
      this.elVideo.classList.remove("hide");
      this.resize();
    }
  }

  resize() {
    if (this.options.resize === "no-resize") return;

    let ww = this.el.clientWidth,
      wh = this.el.clientHeight,
      rw = wh / ww,
      ri = this.options.ratio,
      w,
      h,
      mLeft,
      mTop;

    if (this.options.resize && this.options.resize === "contains") {
      if (rw < ri) {
        w = wh / ri;
        h = wh;
      } else {
        w = ww;
        h = ww * ri;
      }
    } else {
      if (rw > ri) {
        w = wh / ri;
        h = wh;
      } else {
        w = ww;
        h = ww * ri;
      }
    }

    mLeft = ww / 2 - w / 2;
    mTop = wh / 2 - h / 2;

    this.elVideo.style.width = Math.ceil(w) + "px";
    this.elVideo.style.height = Math.ceil(h) + "px";
    this.elVideo.style.left = Math.ceil(mLeft) + "px";
    this.elVideo.style.top = Math.ceil(mTop) + "px";
  }

  destroy() {
    this.removeBuffer();
    this.elVideo.removeEventListener("canplaythrough", this.canplaythrough);
    this.elVideo.removeEventListener("canplay", this.canplay);
    this.elVideo.removeEventListener("loadedmetadata", this.videoMetadataReady);
    this.elVideo.removeEventListener("waiting", this.videoWaiting);
    this.elVideo.removeEventListener("playing", this.videoPlaying);
    this.elVideo.removeEventListener("ended", this.videoEnd);

    //        this.elVideo.remove();

    this.el.removeChild(this.elVideo);
    this.elVideo = null;

    // this.el.innerHTML = '';
  }
}
