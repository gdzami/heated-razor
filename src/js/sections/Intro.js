import SequenceImages from "./_components/SequenceImages";
import Config from "../Config";

export default class Intro {
  constructor() {
    this.s = {
      id: "experience",
      title: "Experience",
      ratioDesktop: 810 / 1440,
      ratioMobile: 700 / 600,
      endAutoplayDesktop: 11,
      endAutoplayMobile: 5,
      endAutoplay: null,
      nbImgsDesktop: 119,
      nbImgsMobile: 59,
      nbImgs: null,
      captionDesktop: [46, 56, 72, 82, 94, 106, 118],
      captionMobile: [23, 30, 36, 41, 47, 53, 58],
      caption: null,
      folder: Config.s.supportsWebp ? "dist/img/intro/_sequences-webp-80" : "dist/img/intro/_sequences-jpg",
      mobile: Config.s.mobile && window.innerWidth <= Config.s.breakpoints.xs ? true : false,
      extension: Config.s.supportsWebp ? ".png.webp" : ".jpg",
      delayTimerStats: 3000
    };

    this.elBody = document.body;

    this.el = document.getElementById(this.s.id);
    this.elCaptions = this.el.querySelector(".captions");
    this.elMenuCaption = this.elCaptions.querySelectorAll("li");
    this.elSequenceCaption = this.el.querySelectorAll(".sequence-caption");
    this.elScrollInfo = this.el.querySelector(".scroll-down");

    this.s.endAutoplay = this.s.mobile ? this.s.endAutoplayMobile : this.s.endAutoplayDesktop;
    this.s.folder = this.s.mobile ? this.s.folder + "-mobile/" : this.s.folder + "/";
    this.s.nbImgs = this.s.mobile ? this.s.nbImgsMobile : this.s.nbImgsDesktop;
    this.s.caption = this.s.mobile ? this.s.captionMobile : this.s.captionDesktop;

    this.nbCaptions = this.s.caption.length;
    this.indexMenu = -1;
    this.indexImg = -1;
    this.scrollHeader = false;
    this.playAutoStart = false;
    this.timerStats = null;
    this.stats = false;

    this.initSequence();
    this.initButtonsCaption();
  }

  //-----------------------------------------------------o public

  resize() {
    this.sequence.resize();
  }

  setScrollHeader(b) {
    if (b && this.el.classList.contains("fixed")) {
      this.el.classList.remove("fixed");
      this.elBody.classList.remove("fixed");
    }
    this.scrollHeader = b;
  }

  update(st, direction) {
    const wh = window.innerHeight;
    const indexImg = this.sequence.getIndexImg();
    const t = this.el.offsetTop;
    const nb = 3; // nb sections of "Experience"

    if (st >= t && st < t + nb * wh && this.timerStats === null && !this.stats) {
      this.sendStats();
    } else if ((st < t || st > t + nb * wh) && this.timerStats !== null) {
      this.clearTimerStats();
    }

    if (st < t + wh && !this.scrollHeader) {
      const directionSequence = this.sequence.getDirection();

      if (indexImg !== this.indexImg) {
        this.updateCaptions(indexImg);
      }

      if (indexImg < this.s.nbImgs - 1 || st < 0) {
        this.addFixed();
      } else if (indexImg === this.s.nbImgs - 1 && (direction === "bottom" || directionSequence === "bottom")) {
        this.removeFixed(directionSequence);
      }

      this.sequence.update(st);
    } else if (st >= t + wh && indexImg !== this.s.nbImgs - 1) {
      this.sequence.resetEnd();
      this.removeFixed();
    }
  }

  //-----------------------------------------------------o private
  initSequence() {
    this.sequence = new SequenceImages(this.el, {
      autoplay: this.s.endAutoplay,
      startLoop: this.s.mobile ? this.s.endAutoplay - 4 : this.s.endAutoplay - 8,
      slow: this.s.mobile ? false : true,
      nbImgs: this.s.nbImgs,
      folder: this.s.folder,
      ratio: this.s.mobile ? this.s.ratioMobile : this.s.ratioDesktop,
      extension: this.s.extension,
      wheel: true,
      scroll: false
    });

    this.sequence.on(this.sequence.EVENT.START_AUTOPLAY, () => {
      this.playAutoStart = true;
    });
  }

  initButtonsCaption() {
    const items = [].slice.call(this.elMenuCaption);
    items.forEach((item, i) => {
      item.setAttribute("data-index", this.s.caption[i]);
      item.querySelector("button").addEventListener("click", event => {
        if (!item.classList.contains("active")) {
          this.playAutoStart = false;
          this.sequence.play(Math.floor(item.getAttribute("data-index")) - 1);
          event.preventDefault();
          event.stopPropagation();
        }
      });
    });

    this.elScrollInfo.addEventListener("click", () => {
      this.playAutoStart = true;
      this.sequence.play(this.s.nbImgs - 1, 65);
      this.elScrollInfo.classList.remove("show");
    });

    if (this.s.mobile) {
      this.elCaptions.classList.add("disabled");
    }
  }

  updateCaptions(indexImg) {
    this.indexImg = indexImg;

    if (indexImg > this.s.endAutoplay + 1) {
      if (this.elScrollInfo.classList.contains("show")) {
        this.elScrollInfo.classList.remove("show");
        this.elCaptions.classList.add("show");
      }
    } else {
      if (!this.elScrollInfo.classList.contains("show")) {
        this.elScrollInfo.classList.add("show");
        this.elCaptions.classList.remove("show");
      }
    }

    for (let i = 0; i < this.nbCaptions; i++) {
      if (indexImg <= this.s.endAutoplay) {
        this.removeActiveMenu(0);
        this.removeShowCaption(0);
        this.indexMenu = -1;
      } else if (indexImg <= this.s.caption[i] && indexImg > this.s.endAutoplay + 1) {
        this.showCaption();
        this.addActiveMenu(i);

        return;
      }
    }
  }

  addActiveMenu(indexCaptionsActive) {
    const modeAuto = this.sequence.getModeAuto();

    if (indexCaptionsActive >= 0 && (modeAuto === null || this.playAutoStart)) {
      const el = this.elMenuCaption[indexCaptionsActive];
      const el2 = this.elSequenceCaption[indexCaptionsActive];

      if (!el.classList.contains("active")) {
        el.classList.add("active");
        el2.classList.add("active");

        if (this.indexMenu !== -1) {
          this.removeActiveMenu(this.indexMenu);
        }
        this.indexMenu = indexCaptionsActive;
      }
    } else if (modeAuto !== null) {
      if (this.indexMenu !== -1) {
        this.removeActiveMenu(this.indexMenu);
      }
    }
  }

  removeActiveMenu(index) {
    if (this.elMenuCaption[index].classList.contains("active")) {
      this.elMenuCaption[index].classList.remove("active");
      this.elSequenceCaption[index].classList.remove("active");
    }
  }

  showCaption() {
    for (let i = 0; i < this.nbCaptions; i++) {
      if (i <= this.indexMenu) {
        this.addShowCaption(i);
      } else {
        this.removeShowCaption(i);
      }
    }
  }

  addShowCaption(index) {
    const el = this.elMenuCaption[index];

    if (!el.classList.contains("show")) {
      el.classList.add("show");
    }
  }

  removeShowCaption(index) {
    const el = this.elMenuCaption[index];

    if (el.classList.contains("show")) {
      el.classList.remove("show");
    }
  }

  addFixed() {
    if (!this.el.classList.contains("fixed") && Config.s.touch) {
      this.el.classList.add("fixed");
      this.elBody.classList.add("fixed");
      this.elBody.scrollTop = 0;
    }
  }

  removeFixed(directionSequence) {
    if (this.el.classList.contains("fixed") && Config.s.touch) {
      this.el.classList.remove("fixed");
      this.elBody.classList.remove("fixed");

      if (directionSequence && directionSequence === "bottom" && this.s.mobile) {
        TweenMax.to(window, 0.4, { scrollTo: { y: this.el.offsetTop + 50 } });
      } else {
        TweenMax.set(window, { scrollTo: { y: 0 } });
      }
    }
  }

  sendStats() {
    this.timerStats = setTimeout(() => {
      window.dataLayer.push({
        event: "VirtualPageview",
        virtualPageURL: location.pathname + location.search + this.s.id,
        virtualPageTitle: "Heated Razor – " + this.s.title
      });
      this.stats = true;
    }, this.s.delayTimerStats);
  }

  clearTimerStats() {
    if (this.timerStats !== null) {
      clearTimeout(this.timerStats);
      this.timerStats = null;
    }
  }
}
