import { Sine, TweenMax } from "gsap";
import Config from "../Config";

export default class Product {
  constructor() {
    this.s = {
      id: "design",
      title: "Design",
      slideRatioWh: Config.s.mobile ? 0.1 : 0.23,
      duration: 1.2,
      opacityOff: 0.3,
      blur: 15,
      delayTimerStats: 3000
    };

    this.el = document.getElementById(this.s.id);
    this.elProducts = this.el.querySelectorAll(".product");

    this.nbProducts = this.elProducts.length;
    this.indexActive = -1;
    this.scrollHeader = false;
    this.timerStats = null;
    this.stats = false;

    this.initPositionProducts();
  }

  //-----------------------------------------------------o public
  update(st) {
    const t = this.el.offsetTop;
    const h = this.el.clientHeight;
    const wh = window.innerHeight;
    let selected;

    if (st >= t && st < t + h && this.timerStats === null && !this.stats) {
      this.sendStats();
    } else if ((st < t || st > t + h) && this.timerStats !== null) {
      this.clearTimerStats();
    }

    if (st > t - wh && st < t + h && !this.scrollHeader) {
      for (let i = this.nbProducts - 1; i >= 0; i--) {
        const el = this.elProducts[i];
        const t1 = t + el.offsetTop - wh * 0.5;
        if (st > t1 && st < t1 + el.clientHeight) {
          if (i !== this.indexActive) {
            this.removeActive();
            this.addActive(el);
            this.indexActive = i;
          }

          selected = i;
          return;
        }
      }

      if (!selected) {
        this.removeActive();
        this.indexActive = -1;
      }
    }
  }

  setScrollHeader(b) {
    if (!b) {
      this.initPositionProducts();
    }
    this.scrollHeader = b;
  }

  //-----------------------------------------------------o private
  initPositionProducts() {
    const len = this.elProducts.length;
    const st = window.pageYOffset || document.documentElement.scrollTop;
    const t = this.el.offsetTop;
    const ww = window.innerWidth;
    const mTop = ww <= Config.s.breakpoints.s ? Config.s.headerHeightMobile : 0;

    for (let i = 0; i < len; i++) {
      const el = this.elProducts[i];
      const t1 = el.offsetTop;

      if (st < t + t1 - mTop || st > t + t1 - mTop + el.clientHeight) {
        this.resetPosition(el);
      } else {
        this.indexActive = i;
        this.addActive(el);
      }
    }
  }

  addActive(el) {
    el.classList.add("active");
    const elPhoto = el.querySelector(".photo");
    const elImg1 = elPhoto.querySelectorAll(".item")[0];
    const elImg2 = elPhoto.querySelectorAll(".item")[1];

    TweenMax.to(el, this.s.duration, { y: "0px", force3D: true, overwrite: true, ease: Sine.easeOut });
    TweenMax.to(elPhoto, this.s.duration, { force3D: true, opacity: 1, ease: Sine.easeOut });

    if (Config.s.isFirefox || Config.s.mobile) {
      TweenMax.to(elImg1, this.s.duration, { y: 0, force3D: true, ease: Sine.easeOut });
      TweenMax.to(elImg2, this.s.duration, { delay: 0.1, y: 0, force3D: true, ease: Sine.easeOut });
    } else {
      TweenMax.to(elImg1, this.s.duration / 1.5, { filter: "blur(0)", y: 0, force3D: true, ease: Sine.easeOut });
      TweenMax.to(elImg2, this.s.duration / 1.5, { delay: 0.1, filter: "blur(0)", y: 0, force3D: true, ease: Sine.easeOut });
    }
  }

  removeActive() {
    if (this.indexActive !== -1) {
      const el = this.elProducts[this.indexActive];
      this.resetPosition(el);
    }
  }

  resetPosition(el) {
    const st = window.pageYOffset || document.documentElement.scrollTop;
    el.classList.remove("active");

    const slide = window.innerHeight * this.s.slideRatioWh;
    const t = this.el.offsetTop;
    const t1 = el.offsetTop;
    const elPhoto = el.querySelector(".photo");
    const elImg1 = elPhoto.querySelectorAll(".item")[0];
    const elImg2 = elPhoto.querySelectorAll(".item")[1];
    const py = st < t + t1 ? slide : -slide;

    TweenMax.to(el, this.s.duration, { y: py + "px", force3D: true, overwrite: true });
    TweenMax.to(elPhoto, this.s.duration, { opacity: this.s.opacityOff });

    if (Config.s.isFirefox || Config.s.mobile) {
      TweenMax.to(elImg1, this.s.duration, { y: py + "px", force3D: true, ease: Sine.easeOut });
      TweenMax.to(elImg2, this.s.duration, { y: py * 1.1 + "px", force3D: true, ease: Sine.easeOut });
    } else {
      TweenMax.to(elImg1, this.s.duration, { filter: "blur(" + this.s.blur + "px)", y: py + "px", force3D: true, ease: Sine.easeOut });
      TweenMax.to(elImg2, this.s.duration, {
        filter: "blur(" + this.s.blur + "px)",
        y: py * 1.1 + "px",
        force3D: true,
        ease: Sine.easeOut
      });
    }
  }

  sendStats() {
    this.timerStats = setTimeout(() => {
      window.dataLayer.push({
        event: "VirtualPageview",
        virtualPageURL: location.pathname + location.search + this.s.id,
        virtualPageTitle: "Heated Razor – " + this.s.title
      });
      this.stats = true;
    }, this.s.delayTimerStats);
  }

  clearTimerStats() {
    if (this.timerStats !== null) {
      clearTimeout(this.timerStats);
      this.timerStats = null;
    }
  }
}
