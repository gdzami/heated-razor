import SequenceImages from "./_components/SequenceImages";
import Config from "../Config";

export default class ProductExplosion {
  constructor() {
    this.s = {
      id: "product-explosion",
      ratioDesktop: 810 / 1440,
      ratioMobile: 700 / 550,
      nbImgsDesktop: 60,
      nbImgsMobile: 30,
      nbImgs: null,
      folder: Config.s.supportsWebp ? "dist/img/product-explosion/_sequences-webp" : "dist/img/product-explosion/_sequences-jpg",
      mobile: Config.s.mobile && window.innerWidth <= Config.s.breakpoints.m ? true : false,
      extension: Config.s.supportsWebp ? ".png.webp" : ".jpg"
    };
    this.s.folder = this.s.mobile ? this.s.folder + "-mobile/" : this.s.folder + "/";
    this.s.nbImgs = this.s.mobile ? this.s.nbImgsMobile : this.s.nbImgsDesktop;

    this.el = document.getElementById(this.s.id);
    this.elTitle = this.el.querySelector("h2");
    this.sequence = null;
    this.scrollTop = 0;
    this.scrollHeader = false;
  }

  //-----------------------------------------------------o public
  resize() {
    if (this.sequence !== null) {
      this.sequence.resize();
    }
  }

  setScrollHeader(b) {
    if (!this.s.mobile) {
      if (b && this.el.classList.contains("fixed")) {
        this.el.classList.remove("fixed");
      } else if (!b && this.sequence !== null) {
        const st = window.pageYOffset || document.documentElement.scrollTop;
        const wh = window.innerHeight;
        const t = this.el.offsetTop;
        if (st >= t + wh) {
          this.sequence.resetEnd();
          this.sequence.s.wheel = false;
          this.sequence.s.scroll = true;
        } else if (st <= t - wh) {
          this.sequence.resetStart();
          this.sequence.s.wheel = true;
          this.sequence.s.scroll = false;
        }
      }
    }

    this.scrollHeader = b;
  }

  update(st) {
    const wh = window.innerHeight;
    const ww = window.innerWidth;
    const t = this.el.offsetTop;
    const marginTopLimit = ww > wh ? wh * 0.2 : wh * 0.5;

    let direction = null;
    if (st > this.scrollTop) {
      direction = "bottom";
    } else if (st < this.scrollTop) {
      direction = "top";
    }

    this.scrollTop = st;

    if (st > wh / 2 && this.sequence === null) {
      this.initSequence();
      this.resize();
    } else if (this.sequence !== null) {
      const indexImg = this.sequence.getIndexImg();
      const directionSequence = this.sequence.getDirection();

      if (!this.s.mobile && !this.scrollHeader) {
        if (st < t - wh && indexImg !== 0) {
          this.sequence.resetStart();
          this.sequence.s.wheel = true;
          this.sequence.s.scroll = false;
        }

        if (direction === "bottom" || directionSequence === "bottom") {
          if (st >= t && st <= t + marginTopLimit && !this.el.classList.contains("fixed") && indexImg < this.s.nbImgs - 1) {
            this.sequence.s.wheel = true;
            this.sequence.s.scroll = false;
            this.addFixed();
          } else if (st > t + marginTopLimit && indexImg !== this.s.nbImgs - 1 && !this.el.classList.contains("fixed")) {
            this.sequence.resetEnd();
          } else if (indexImg === this.s.nbImgs - 1 && this.el.classList.contains("fixed")) {
            this.removeFixed(true);
          } else if (st > t + wh && this.el.classList.contains("fixed")) {
            this.sequence.resetEnd();
          }
        } else if (direction === "top" || directionSequence === "top") {
          if (!this.el.classList.contains("fixed") && indexImg !== 0) {
            this.sequence.s.wheel = false;
            this.sequence.s.scroll = true;
          } else if ((indexImg === 0 || directionSequence === null) && this.el.classList.contains("fixed")) {
            this.removeFixed();
          }
        }
      }

      if (!this.scrollHeader) {
        this.sequence.update(st, direction);
        this.animateTitle(indexImg);
      }
    }
  }

  //-----------------------------------------------------o private
  initSequence() {
    this.sequence = new SequenceImages(this.el, {
      autoplay: 0,
      nbImgs: this.s.nbImgs,
      folder: this.s.folder,
      ratio: this.s.mobile ? this.s.ratioMobile : this.s.ratioDesktop,
      extension: this.s.extension,
      wheel: this.s.mobile ? false : true,
      scroll: this.s.mobile ? true : false
    });
  }

  animateTitle(indexImg) {
    const index = Math.floor(this.s.nbImgs / 2);

    if (indexImg > index && !this.elTitle.classList.contains("active")) {
      this.elTitle.classList.add("active");
    } else if (indexImg < index && this.elTitle.classList.contains("active")) {
      this.elTitle.classList.remove("active");
    }
  }

  addFixed() {
    this.el.classList.add("fixed");
    this.scrollTop = this.el.offsetTop;
    TweenMax.set(window, { scrollTo: { y: this.el.offsetTop } });
  }

  removeFixed(b) {
    this.el.classList.remove("fixed");
    this.scrollTop = this.el.offsetTop;

    if (b) {
      TweenMax.set(window, { scrollTo: { y: this.el.offsetTop } });
    }
  }
}
