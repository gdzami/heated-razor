import Config from "../Config";
import { Power2 } from "gsap";

export default class Awards {
  constructor() {
    this.s = {
      slide: 25,
      duration: 2,
      durationReview: 5
    };

    this.el = document.getElementById("awards");
    this.elImg = this.el.querySelector("img.bkg");
    this.elReviews = this.el.querySelector("ul.reviews");
    this.elBar = this.el.querySelector(".bar-loader");
    this.elBarProgress = this.el.querySelector(".bar-progress");

    this.indexItem = 0;
    this.nbItems = this.elReviews.querySelectorAll("li").length;
    this.pause = true;

    this.init();
  }

  //-----------------------------------------------------o public
  update(st, direction) {
    const wh = window.innerHeight;

    if (st >= this.el.offsetTop - wh * 0.3 && st < this.el.offsetTop + wh * 0.9) {
      if (!Config.s.mobile) {
        this.animateImage(direction);
      }

      if (this.pause) {
        this.pause = false;
        this.startLoaderProgress();
      }
    } else if (!this.pause) {
      this.pause = true;
      this.pauseCaroussel();
    }
  }

  //-----------------------------------------------------o private
  init() {
    const links = [].slice.call(this.elReviews.querySelectorAll("a"));

    links.forEach(link => {
      link.addEventListener("click", () => {
        dataLayer.push({
          event: "HRButtonClick",
          eventCategory: "button",
          eventAction: "click",
          eventLabel: link.getAttribute("href")
        });
      });
    });
  }

  startLoaderProgress() {
    const w = this.elBar.clientWidth;
    let d = this.s.durationReview;

    if (this.elBarProgress._gsTransform) {
      const px = Math.abs(this.elBarProgress._gsTransform.x);
      d = (px * this.s.durationReview) / w;
    }

    TweenMax.to(this.elBarProgress, d, {
      x: 0,
      force3D: true,
      onComplete: () => {
        this.updateReview();
      }
    });
  }

  pauseCaroussel() {
    TweenMax.killTweensOf(this.elBarProgress);
  }

  updateReview() {
    const el = this.elReviews.querySelectorAll("li")[this.indexItem];
    const w = this.elBar.clientWidth;

    if (this.indexItem < this.nbItems - 1) {
      this.indexItem++;
    } else {
      this.indexItem = 0;
    }

    const elNew = this.elReviews.querySelectorAll("li")[this.indexItem];
    el.classList.remove("show");
    elNew.classList.add("show");

    TweenMax.to(this.elBarProgress, 0.6, { x: w + "px", force3D: true, ease: Sine.easeOut });

    TweenMax.fromTo(
      elNew,
      0.7,
      { opacity: 0 },
      {
        opacity: 1,
        force3D: true,
        ease: Sine.easeOut,
        onComplete: () => {
          TweenMax.set(this.elBarProgress, { x: -w + "px" });
          this.startLoaderProgress();
        }
      }
    );
  }

  animateImage(direction) {
    let t = 0;
    if (direction === "bottom") {
      t = this.s.slide;
    } else if (direction === "top") {
      t = -this.s.slide;
    }

    TweenMax.to(this.elImg, this.s.duration, { y: Math.floor(t) + "px", force3D: true, ease: Sine.easeOut });
  }
}
