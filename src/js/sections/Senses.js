export default class Senses {
  constructor() {
    this.s = {
      id: "senses"
    };

    this.el = document.getElementById(this.s.id);
    this.elVideo = this.el.querySelector("video");
    this.elTitle = this.el.querySelector("h2");
    this.elTitleShadow1 = this.el.querySelector("div.shadow span.line-1");
    this.elTitleShadow2 = this.el.querySelector("div.shadow span.line-2");

    this.playing = false;
    this.scrollHeader = false;
  }

  //-----------------------------------------------------o public
  resize() {
    let ww = this.el.clientWidth,
      wh = this.el.clientHeight,
      rw = wh / ww,
      ri = 9 / 16,
      w,
      h,
      mLeft,
      mTop;

    if (rw > ri) {
      w = wh / ri;
      h = wh;
    } else {
      w = ww;
      h = ww * ri;
    }
    mLeft = ww / 2 - w / 2;
    mTop = wh / 2 - h / 2;

    this.elVideo.style.width = Math.ceil(w) + "px";
    this.elVideo.style.height = Math.ceil(h) + "px";
    this.elVideo.style.left = Math.ceil(mLeft) + "px";
    this.elVideo.style.top = Math.ceil(mTop) + "px";
  }

  update(st) {
    const wh = window.innerHeight;
    if (this.scrollHeader) return;

    if (st > this.el.offsetTop - wh * 0.2 && st < this.el.offsetTop + wh && !this.playing) {
      this.elVideo.play();
      this.playing = true;
      this.elTitle.classList.add("show");
      this.showShadow();
    } else if (
      (st < this.el.offsetTop - this.el.clientHeight || st > this.el.offsetTop + this.el.clientHeight) &&
      this.elTitle.classList.contains("show")
    ) {
      TweenMax.killTweensOf(this.elTitleShadow1);
      TweenMax.killTweensOf(this.elTitleShadow2);
      this.elTitleShadow1.removeAttribute("style");
      this.elTitleShadow2.removeAttribute("style");

      this.elTitle.classList.remove("show");
      this.elVideo.currentTime = 0;
      this.elVideo.pause();
      this.playing = false;
    }
  }

  setScrollHeader(b) {
    this.scrollHeader = b;
  }

  //-----------------------------------------------------o private
  showShadow() {
    TweenMax.to(this.elTitleShadow1, 0.3, {
      opacity: 1,
      delay: 0.3,
      force3D: true,
      ease: Power2.easeOut,
      onComplete: () => {
        TweenMax.to(this.elTitleShadow1, 0.3, { opacity: 0.1, delay: 0.8, force3D: true, ease: Power2.easeOut });
      }
    });
    TweenMax.to(this.elTitleShadow2, 0.3, {
      opacity: 1,
      delay: 0.6,
      force3D: true,
      ease: Power2.easeOut,
      onComplete: () => {
        TweenMax.to(this.elTitleShadow2, 0.3, { opacity: 0.1, delay: 0.5, force3D: true, ease: Power2.easeOut });
      }
    });
  }
}
