import Config from "../Config";

export default class ProductWarmth {
  constructor() {
    this.s = {
      id: "technology",
      title: "Technology",
      ratioImg: 790 / 1400,
      ratioImgPortrait: 929 / 550,
      durationMoveImg: 2,
      delayTimerStats: 3000
    };

    this.el = document.getElementById(this.s.id);
    this.elWrap = this.el.querySelector(".wrap");

    this.elBkgOff = this.el.querySelector(".off");
    this.elBkgOn = this.el.querySelector(".on");
    this.elImgOff = this.elBkgOff.querySelector("img");
    this.elImgOn = this.elBkgOn.querySelector("img");

    this.elTitle = this.el.querySelector("h2");
    this.elTitleChildren = this.elTitle.querySelectorAll("span");

    this.index = 0;
    this.direction = null;
    this.scrollHeader = false;
    this.timerStats = null;
    this.stats = false;

    this.init();
  }

  //-----------------------------------------------------o public
  update(st, direction) {
    const h = this.el.clientHeight;
    const t = this.el.offsetTop;
    const ww = window.innerWidth;
    const mTop = ww <= Config.s.breakpoints.s ? Config.s.headerHeightMobile : 0;
    const wh = window.innerHeight - mTop;
    const nb = 3; // nb sections of "technology"

    this.direction = direction;

    if (st >= t && st < t + nb * wh && this.timerStats === null && !this.stats) {
      this.sendStats();
    } else if ((st < t || st > t + nb * wh) && this.timerStats !== null) {
      this.clearTimerStats();
    }

    if (this.direction === "bottom") {
      if (st > t && st < t + h - wh) {
        if (!this.el.classList.contains("fixed")) {
          this.addFixed(this.direction);
        }

        if (this.index === 0) {
          this.index = 1;
          this.animateImage(this.direction);
          this.switchImage(this.s.durationMoveImg);
        }
      } else if (st >= t + h - wh && this.el.classList.contains("fixed")) {
        this.removeFixed(this.direction);
      }
    } else if (this.direction === "top") {
      if (st > t && st < t + h - wh) {
        if (!this.el.classList.contains("fixed")) {
          this.addFixed(this.direction);
        }
        if (this.index === 1) {
          this.index = 0;
          this.animateImage(this.direction);
          this.switchImage(this.s.durationMoveImg);
        }
      } else if (st < t + h - wh && this.el.classList.contains("fixed")) {
        this.removeFixed();
      }
    }
  }

  resize() {
    this.elImgOff.removeAttribute("style");
    let ww = this.elWrap.clientWidth,
      wh = this.elWrap.clientHeight,
      rw = wh / ww,
      ri = this.elImgOff.clientWidth >= this.elImgOff.clientHeight ? this.s.ratioImg : this.s.ratioImgPortrait,
      w,
      h,
      mLeft,
      mTop;

    if (rw > ri) {
      w = wh / ri;
      h = wh;
    } else {
      w = ww;
      h = ww * ri;
    }
    mLeft = ww / 2 - w / 2;
    mTop = wh / 2 - h / 2;

    this.elImgOff.style.width = Math.ceil(w) + "px";
    this.elImgOff.style.height = Math.ceil(h) + "px";
    this.elImgOff.style.left = Math.ceil(mLeft) + "px";
    this.elImgOff.style.top = Math.ceil(mTop) + "px";

    this.elImgOn.style.width = Math.ceil(w) + "px";
    this.elImgOn.style.height = Math.ceil(h) + "px";
    this.elImgOn.style.left = Math.ceil(mLeft) + "px";
    this.elImgOn.style.top = Math.ceil(mTop) + "px";
  }

  setScrollHeader(b) {
    if (!b) {
      this.el.classList.remove("fixed");
      this.init();
    }
    this.scrollHeader = b;
  }

  //-----------------------------------------------------o private
  init() {
    const wh = window.innerHeight;
    const h = this.el.clientHeight;
    const t = this.el.offsetTop;
    const st = window.pageYOffset || document.documentElement.scrollTop;

    if (st > t + h - wh) {
      this.el.classList.add("bottom");
      this.index = 1;
      this.switchImage(0);
    } else {
      this.el.classList.remove("bottom");
      this.index = 0;
      this.switchImage(0);
    }
  }

  switchImage(duration) {
    const h = 25;
    if (this.index === 1) {
      this.elBkgOff.classList.remove("active");
      this.elBkgOn.classList.add("active");
      this.elTitleChildren[0].classList.remove("active");
      this.elTitleChildren[1].classList.add("active");
      TweenMax.to(this.elTitle, duration, { y: -h + "px", force3D: true, ease: Sine.easeOut });
    } else {
      this.elBkgOff.classList.add("active");
      this.elBkgOn.classList.remove("active");
      this.elTitleChildren[1].classList.remove("active");
      this.elTitleChildren[0].classList.add("active");
      TweenMax.to(this.elTitle, this.s.durationMoveImg, { y: h + "px", force3D: true, ease: Sine.easeOut });
    }
  }

  animateImage(direction) {
    let t = 0;
    let dist = this.elImgOff.clientHeight - this.elBkgOff.clientHeight;
    if (dist < 0) {
      t = 0;
    } else if (direction === "bottom") {
      t = -dist / 2;
    } else if (direction === "top") {
      t = dist / 2;
    }

    TweenMax.to(this.elImgOff, this.s.durationMoveImg, { y: Math.floor(t) + "px", force3D: true, ease: Sine.easeOut });
    TweenMax.to(this.elImgOn, this.s.durationMoveImg, { y: Math.floor(t) + "px", force3D: true, ease: Sine.easeOut });
  }

  addFixed(direction) {
    this.el.classList.add("fixed");

    if (direction === "top") {
      this.el.classList.add("bottom");
    } else {
      this.el.classList.remove("bottom");
    }
  }

  removeFixed(direction) {
    this.el.classList.remove("fixed");
    if (direction === "bottom") {
      this.el.classList.add("bottom");
    } else {
      this.el.classList.remove("bottom");
    }
  }

  sendStats() {
    this.timerStats = setTimeout(() => {
      window.dataLayer.push({
        event: "VirtualPageview",
        virtualPageURL: location.pathname + location.search + this.s.id,
        virtualPageTitle: "Heated Razor – " + this.s.title
      });
      this.stats = true;
    }, this.s.delayTimerStats);
  }

  clearTimerStats() {
    if (this.timerStats !== null) {
      clearTimeout(this.timerStats);
      this.timerStats = null;
    }
  }
}
