import SequenceImages from "./_components/SequenceImages";
import Config from "../Config";
import { EventEmitter } from "events";

export default class Shop extends EventEmitter {
  constructor() {
    super(...arguments);

    this.EVENT = {
      OPEN_POPIN: "openPopin"
    };

    this.s = {
      id: "shop",
      title: "Buy",
      ratioDesktop: 810 / 1440,
      ratioMobile: 700 / 550,
      time: 200,
      nbImgs: 16,
      folder: Config.s.supportsWebp ? "dist/img/shop/_sequences-webp" : "dist/img/shop/_sequences-jpg",
      mobile: Config.s.mobile && window.innerWidth <= Config.s.breakpoints.xs ? true : false,
      extension: Config.s.supportsWebp ? ".png.webp" : ".jpg",
      delayTimerStats: 3000
    };

    this.s.folder = this.s.mobile ? this.s.folder + "-mobile/" : this.s.folder + "/";

    this.el = document.getElementById(this.s.id);
    this.elTitle = this.el.querySelector(".title");
    this.elSequence = this.el.querySelector(".sequence");
    this.elRazor = this.el.querySelector(".razor");
    this.elDock = this.el.querySelector(".dock");

    this.elPlug1 = this.el.querySelector(".plug-1");
    this.elPlug2 = this.el.querySelector(".plug-2");
    this.elBtnClub = this.el.querySelector(".btn--club");
    this.elBtnBuy = this.el.querySelector(".btn--buy");
    this.elBtnBladesClub = this.el.querySelector(".btn--club-blades");
    this.elBtnBladesBuy = this.el.querySelector(".btn--buy-blades");

    this.playing = false;
    this.timer = null;
    this.sequence = null;

    this.timerStats = null;
    this.stats = false;

    this.initBtns();
  }

  //-----------------------------------------------------o public
  resize() {
    if (this.sequence !== null) {
      this.sequence.resize();
    }
  }

  setScrollHeader(b, id) {
    //this.scrollHeader = b;

    if (b && this.el.classList.contains("fixed") && id !== "shop") {
      this.el.classList.remove("fixed");
    }
  }

  update(st, direction) {
    const wh = window.innerHeight;
    const t = this.el.offsetTop;

    if (st >= t && st < t + wh && this.timerStats === null && !this.stats) {
      this.sendStats();
    } else if ((st < t || st > t + wh) && this.timerStats !== null) {
      this.clearTimerStats();
    }

    if (st > wh / 2 && this.sequence === null) {
      this.initSequence();
      this.resize();
    } else if (this.sequence !== null) {
      if (st < t - wh) {
        if (this.elSequence.classList.contains("show")) {
          this.reset();
        }
      } else {
        const indexImg = this.sequence.getIndexImg();
        this.displayContent(indexImg);
        this.sequence.update(st, direction);

        if (direction !== this.direction) {
          this.playing = false;
        }

        if (st > t - 2) {
          if (!this.el.classList.contains("fixed") && indexImg === 0) {
            this.addFixed();
          }
          this.startForward(indexImg);
        } else {
          this.startRewind(indexImg);
        }

        this.direction = direction;
      }
    }
  }

  //-----------------------------------------------------o private
  initBtns() {
    this.elBtnClub.addEventListener("click", () => {
      this.emit(this.EVENT.OPEN_POPIN, { type: "club" });
      fbq("track", "ViewContent", {
        content_name: this.elBtnClub.getAttribute("data-label-stats"),
        content_category: "Heated Razor",
        value: this.elBtnClub.getAttribute("data-price-stats"),
        currency: document.body.dataset.currency
      });

      window.dataLayer.push({
        event: "HRButtonClick",
        eventCategory: "button",
        eventAction: "click",
        eventLabel: "subscribe"
      });
    });

    this.elBtnBuy.addEventListener("click", () => {
      this.emit(this.EVENT.OPEN_POPIN, { type: "buy" });
      fbq("track", "ViewContent", {
        content_name: this.elBtnBuy.getAttribute("data-label-stats"),
        content_category: "Heated Razor",
        value: this.elBtnBuy.getAttribute("data-price-stats"),
        currency: document.body.dataset.currency
      });

      window.dataLayer.push({
        event: "HRButtonClick",
        eventCategory: "button",
        eventAction: "click",
        eventLabel: "buy_now"
      });
    });

    if (this.elBtnBladesBuy) {
      this.elBtnBladesBuy.addEventListener("click", () => {
        fbq("track", "ViewContent", {
          content_name: this.elBtnBladesBuy.getAttribute("data-label-stats"),
          content_category: "Heated Razor",
          currency: document.body.dataset.currency
        });

        window.dataLayer.push({
          event: "HRButtonClick",
          eventCategory: "button",
          eventAction: "click",
          eventLabel: "buy_blades"
        });
      });
    }

    if (this.elBtnBladesClub) {
      this.elBtnBladesClub.addEventListener("click", () => {
        fbq("track", "ViewContent", {
          content_name: this.elBtnBladesClub.getAttribute("data-label-stats"),
          content_category: "Heated Razor",
          currency: document.body.dataset.currency
        });

        window.dataLayer.push({
          event: "HRButtonClick",
          eventCategory: "button",
          eventAction: "click",
          eventLabel: "subscribe_blades"
        });
      });
    }
  }

  initSequence() {
    this.sequence = new SequenceImages(this.el, {
      autoplay: 0,
      ratio: this.s.mobile ? this.s.ratioMobile : this.s.ratioDesktop,
      nbImgs: this.s.nbImgs,
      folder: this.s.folder,
      extension: this.s.extension,
      wheel: false,
      scroll: false
    });
  }

  startForward(indexImg) {
    if (indexImg === 0) {
      if (!this.elSequence.classList.contains("show") && this.timer === null) {
        this.startTimerShop();
      } else if (this.elSequence.classList.contains("show") && !this.playing) {
        this.play(this.s.nbImgs - 1);
      }
    }

    if (indexImg === this.s.nbImgs - 1) {
      this.playing = false;
      this.removeFixed();
    }
  }

  startRewind(indexImg) {
    if (indexImg === this.s.nbImgs - 1 && !this.playing) {
      this.play(0);
    }

    if (indexImg === 0) {
      this.playing = false;
      this.removeFixed();
    }
  }

  startTimerShop() {
    this.resetTimer();

    this.timer = setTimeout(() => {
      this.elSequence.classList.add("show");
      this.elRazor.classList.add("hide");
      this.play(this.s.nbImgs - 1);
      this.timer = null;
    }, this.s.time);
  }

  resetTimer() {
    if (this.timer !== null) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  play(t) {
    this.sequence.play(t);
    this.playing = true;
  }

  displayContent(indexImg) {
    const ww = window.innerWidth;

    if (indexImg >= Math.floor(this.s.nbImgs / 2)) {
      if (!this.elDock.classList.contains("hide")) {
        this.elDock.classList.add("hide");
        this.elPlug1.classList.add("hide");
        this.elPlug2.classList.add("hide");
        this.elTitle.classList.add("show");
      }
      /*if (this.elBtnBuy.classList.contains("btn--white")) {
        this.elBtnBuy.classList.remove("btn--white");
        this.elBtnClub.classList.remove("btn--white");
        if (this.elBtnBladesClub) {
          this.elBtnBladesClub.classList.remove("btn--white");
        }
        if (this.elBtnBladesBuy) {
          this.elBtnBladesBuy.classList.remove("btn--white");
        }
      }*/
    } else if (ww > Config.s.breakpoints.xxs) {
      this.showDrawing();
    } else if (ww <= Config.s.breakpoints.xxs) {
      /*if (!this.elBtnBuy.classList.contains("btn--white")) {
        this.elBtnBuy.classList.add("btn--white");
        this.elBtnClub.classList.add("btn--white");
        if (this.elBtnBladesClub) {
          this.elBtnBladesClub.classList.add("btn--white");
        }
        if (this.elBtnBladesBuy) {
          this.elBtnBladesBuy.classList.add("btn--white");
        }
      }*/
    }
  }

  showDrawing() {
    if (this.elDock.classList.contains("hide")) {
      this.elDock.classList.remove("hide");
      this.elPlug1.classList.remove("hide");
      this.elPlug2.classList.remove("hide");
      this.elTitle.classList.remove("show");
    }
  }

  reset() {
    this.elSequence.classList.remove("show");
    this.elRazor.classList.remove("hide");
    this.resetTimer();
    this.sequence.resetStart();
    this.showDrawing();
    this.playing = false;
    this.removeFixed();
  }

  addFixed() {
    this.el.classList.add("fixed");
  }

  removeFixed() {
    if (this.el.classList.contains("fixed")) {
      this.el.classList.remove("fixed");
      const t = this.el.offsetTop;
      TweenMax.set(window, { scrollTo: { y: t } });
    }
  }

  sendStats() {
    this.timerStats = setTimeout(() => {
      window.dataLayer.push({
        event: "VirtualPageview",
        virtualPageURL: location.pathname + location.search + this.s.id,
        virtualPageTitle: "Heated Razor – " + this.s.title
      });
      this.stats = true;
    }, this.s.delayTimerStats);
  }

  clearTimerStats() {
    if (this.timerStats !== null) {
      clearTimeout(this.timerStats);
      this.timerStats = null;
    }
  }
}
