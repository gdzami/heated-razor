import { TweenMax } from "gsap/TweenMax";
import Config from "../Config";

export default class ProductShaving {
  constructor() {
    this.s = {
      id: "product-shaving",
      durationMask: 0.8
    };

    this.el = document.getElementById(this.s.id);
    this.elWrap = this.el.querySelector(".wrap");

    this.elImg1 = this.el.querySelector(".bkg-1 div.img");
    this.elBkg2 = this.el.querySelector(".bkg-2");
    this.elLineH = this.el.querySelector(".chart-line-h");
    this.elLineV = this.el.querySelector(".chart-line-v");
    this.elChartRed = this.el.querySelector(".chart-line-red");
    this.elChartBlue = this.el.querySelector(".chart-line-blue");

    this.direction = null;
    this.mvt = false;
    this.index = 0;
    this.scrollHeader = false;

    this.init();
  }

  //-----------------------------------------------------o public
  update(st, direction) {
    const h = this.el.clientHeight;
    const t = this.el.offsetTop;
    const ww = window.innerWidth;
    const mTop = ww <= Config.s.breakpoints.s ? Config.s.headerHeightMobile : 0;
    const wh = window.innerHeight - mTop;

    this.direction = direction;

    if (this.direction === "bottom") {
      if (st > t && st < t + h - wh) {
        if (!this.el.classList.contains("fixed")) {
          this.addFixed(this.direction);
        }

        if (this.index === 0) {
          this.beforeAnimate();
        }
      } else if (st >= t + h - wh && this.el.classList.contains("fixed")) {
        this.removeFixed(this.direction);
        this.scaleHeat(true);
      } else if (st >= t + h - wh * 0.8 && !this.el.classList.contains("fixed") && !this.elImg1.classList.contains("hide")) {
        this.displayHeat(false);
      }
    } else if (this.direction === "top") {
      if (st > t && st < t + h - wh) {
        if (!this.el.classList.contains("fixed")) {
          this.addFixed(this.direction);
          this.scaleHeat(false);
        }
        if (this.index === 1) {
          this.beforeAnimate();
        }
      } else if (st < t + h - wh && this.el.classList.contains("fixed")) {
        this.removeFixed();
      }
    }
  }

  setScrollHeader(b) {
    if (!b) {
      this.el.classList.remove("fixed");
      this.init();
    }

    this.scrollHeader = b;
  }

  //-----------------------------------------------------o private
  init() {
    const wh = window.innerHeight;
    const h = this.el.clientHeight;
    const t = this.el.offsetTop;
    const st = window.pageYOffset || document.documentElement.scrollTop;

    if (st > t + h - wh) {
      this.el.classList.add("bottom");
      this.showChart(0);
      this.index = 1;
      TweenMax.set(this.elBkg2, { height: 0 });
    } else {
      this.index = 0;
      this.el.classList.remove("bottom");
      this.hideChart();
      TweenMax.set(this.elBkg2, { height: wh + "px" });
    }
  }

  beforeAnimate() {
    if (this.direction === "bottom" && this.index === 0) {
      this.index++;

      this.animate(0);
    } else if (this.direction === "top" && this.index === 1) {
      this.index--;

      const h = window.innerHeight;
      this.animate(h);
    }
  }

  animate(h, d) {
    if (this.index === 1) {
      this.showChart();
    }

    const duration = d === 0 ? d : this.s.durationMask;

    TweenMax.to(this.elBkg2, duration, {
      height: h + "px",
      ease: Power2.easeOut,
      onComplete: () => {
        if (this.index === 0) {
          this.hideChart();
        }
      }
    });
  }

  showChart(d) {
    if (d === 0) {
      TweenMax.set(this.elLineH, { x: "0px", opacity: 1, force3D: true, ease: Sine.easeOut });
      TweenMax.set(this.elLineV, { y: "0px", opacity: 1, force3D: true, ease: Sine.easeOut });
      TweenMax.set(this.elChartRed, { width: "100%", overwrite: true, ease: Sine.easeOut });
      TweenMax.set(this.elChartBlue, { width: "100%", force3D: true, ease: Sine.easeOut });
    } else {
      TweenMax.to(this.elLineH, 0.3, { x: "0px", opacity: 1, delay: 0.5, force3D: true, ease: Sine.easeOut });
      TweenMax.to(this.elLineV, 0.3, { y: "0px", opacity: 1, delay: 0.3, force3D: true, ease: Sine.easeOut });
      TweenMax.to(this.elChartRed, 1, { width: "100%", delay: 1, overwrite: true, ease: Sine.easeOut });
      TweenMax.to(this.elChartBlue, 1, { width: "100%", delay: 1.3, force3D: true, ease: Sine.easeOut });
    }
  }

  hideChart() {
    TweenMax.killTweensOf(this.elImg1);
    TweenMax.killTweensOf(this.elLineH);
    TweenMax.killTweensOf(this.elLineV);
    TweenMax.killTweensOf(this.elChartRed);
    TweenMax.killTweensOf(this.elChartBlue);
    TweenMax.set(this.elLineH, { x: -this.elLineH.clientWidth + "px", opacity: 0.8 });
    TweenMax.set(this.elLineV, { y: this.elLineH.clientHeight + "px", opacity: 0.8 });
    TweenMax.set(this.elChartRed, { width: "0" });
    TweenMax.set(this.elChartBlue, { width: "0" });
    TweenMax.set(this.elImg1, { scaleX: "1", scaleY: "1", opacity: 1 });
  }

  scaleHeat(scale) {
    const ww = window.innerWidth;
    const wh = window.innerHeight;

    const scX = Config.s.mobile && wh > ww ? 1.2 : 2.5;
    this.elImg1.classList.remove("hide");

    if (scale) {
      TweenMax.to(this.elImg1, 0.6, { scaleX: scX, scaleY: "2.5", opacity: 1, force3D: true, ease: Sine.easeOut });
    } else {
      TweenMax.to(this.elImg1, 0.5, { scaleX: "1", scaleY: "1", opacity: 1, force3D: true, ease: Sine.easeOut });
    }
  }

  displayHeat(show) {
    const ww = window.innerWidth;
    const wh = window.innerHeight;
    const scX = Config.s.mobile && wh > ww ? 1.5 : 2.5;

    if (!show) {
      this.elImg1.classList.add("hide");
      TweenMax.to(this.elImg1, 0.7, { scaleX: scX, scaleY: scX * 2, opacity: 0, force3D: true, ease: Sine.easeOut });
    } else {
      this.elImg1.classList.remove("hide");
      TweenMax.to(this.elImg1, 0.3, { scaleX: scX, scaleY: scX * 2, opacity: 1, force3D: true, ease: Sine.easeOut });
    }
  }

  addFixed(direction) {
    this.el.classList.add("fixed");

    if (direction === "top") {
      this.el.classList.add("bottom");
    } else {
      this.el.classList.remove("bottom");
    }
  }

  removeFixed(direction) {
    this.el.classList.remove("fixed");
    if (direction === "bottom") {
      this.el.classList.add("bottom");
    } else {
      this.el.classList.remove("bottom");
    }
  }
}
