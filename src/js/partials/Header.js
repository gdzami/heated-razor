import { EventEmitter } from "events";
import { TweenMax, ScrollToPlugin } from "gsap/all";
import { Power2 } from "gsap";

const plugins = [ScrollToPlugin];
import Config from "../Config";

export default class Header extends EventEmitter {
  constructor() {
    super(...arguments);

    this.EVENT = {
      SCROLL: "scroll"
    };
    this.s = {};

    this.elMain = document.getElementById("main");
    this.elFooter = document.getElementById("footer");
    this.elAwards = document.getElementById("awards");
    this.elShop = document.getElementById("shop");

    this.el = document.getElementById("header");

    this.elNav = this.el.querySelector(".menu");
    this.elBurger = this.el.querySelector(".btn-burger");
    this.elAnchors = this.el.querySelectorAll("a.anchor");

    this.timer = null;

    this.initBurger();
    this.initNav();
  }

  //-----------------------------------------------------o public scroll
  update(st) {
    const wh = window.innerHeight;

    const items = [].slice.call(this.elAnchors);

    items.forEach((item, i) => {
      const href = item.getAttribute("href");
      const id = href.substr(1);
      const el = document.getElementById(id);

      let t = el.offsetTop;

      if (st >= t && !item.classList.contains("active")) {
        this.resetCssSubmenu();
        item.classList.add("active");
      } else if (st < t && item.classList.contains("active")) {
        item.classList.remove("active");
      }

      if (st > this.elAwards.offsetTop && item.classList.contains("active")) {
        item.classList.remove("active");
      }
    });

    // header black
    if (
      st > this.elFooter.offsetTop - this.el.clientHeight &&
      !this.elShop.classList.contains("fixed") &&
      !this.el.classList.contains("black")
    ) {
      this.el.classList.add("black");
    } else if (st < this.elFooter.offsetTop - this.el.clientHeight && this.el.classList.contains("black")) {
      this.el.classList.remove("black");
    }
  }

  //-----------------------------------------------------o private
  initBurger() {
    this.elBurger.addEventListener("click", event => {
      if (this.elNav.classList.contains("show")) {
        this.elNav.classList.remove("show");
        this.elBurger.classList.remove("close");
      } else {
        this.elNav.classList.add("show");
        TweenLite.fromTo(this.elNav, 0.3, { opacity: 0 }, { opacity: 1, ease: Power2.easeOut });

        this.elBurger.classList.add("close");
      }
    });
  }

  initNav() {
    const items = [].slice.call(this.elAnchors);

    items.forEach((item, i) => {
      item.addEventListener("click", event => {
        const href = item.getAttribute("href");
        const id = href.substr(1);

        this.goto(id);

        if (id === "shop") {
          window.dataLayer.push({
            event: "HRButtonClick",
            eventCategory: "button",
            eventAction: "click",
            eventLabel: "acheter_buy_now"
          });
        }

        event.preventDefault();
        return false;
      });
    });
  }

  goto(id) {
    const speed = Config.s.mobile ? 0 : 0.5;
    const el = document.getElementById(id);
    const t = el.offsetTop;
    this.elNav.classList.remove("show");
    this.elBurger.classList.remove("close");
    this.clearTimer();

    this.emit(this.EVENT.SCROLL, { status: true, id: id });

    if (speed > 0) {
      TweenMax.to(window, speed, { scrollTo: { y: t }, ease: Power2.easeOut });
    } else {
      TweenMax.set(window, { scrollTo: { y: t } });
    }

    this.timer = setTimeout(() => {
      this.emit(this.EVENT.SCROLL, { status: false, id: id });
      this.timer = null;
    }, speed * 1000);
  }

  resetCssSubmenu() {
    const items = [].slice.call(this.elNav.querySelectorAll("a"));

    items.forEach((item, i) => {
      if (item.classList.contains("active")) {
        item.classList.remove("active");
      }
    });
  }

  clearTimer() {
    if (this.timer !== null) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }
}
