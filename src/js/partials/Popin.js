import Config from "../Config";

export default class Popin {
  constructor() {
    this.s = {};

    this.el = document.getElementById("popin");
    this.elBkg = this.el.querySelector(".bkg");
    this.elPopinClub = this.el.querySelector(".popin-club");
    this.elPopinBuy = this.el.querySelector(".popin-buy");
    this.elBtnClose1 = this.elPopinClub.querySelector(".btn-close");
    this.elBtnClose2 = this.elPopinBuy.querySelector(".btn-close");

    this.elBtnsBuy = this.elPopinBuy.querySelectorAll(".btn--buy");
    this.elBtnClub = this.elPopinBuy.querySelector(".btn--club");
    this.elShaving = this.elPopinClub.querySelector(".nb-shaving");
    this.elBtnValidateClub = this.elPopinClub.querySelector(".btn--validate");

    this.initBtns();
  }

  //-----------------------------------------------------o public scroll

  show(type) {
    this.el.classList.remove("hidden");
    this.el.scrollTop = 0;

    if (type === "club") {
      this.showPopinClub();
    } else {
      this.showPopinBuy();
    }

    TweenLite.fromTo(this.elBkg, 0.1, { opacity: 0 }, { opacity: 1, ease: Power2.easeOut });
    TweenLite.fromTo(this.el, 0.25, { opacity: 0 }, { opacity: 1, ease: Power2.easeOut });
  }

  //-----------------------------------------------------o private
  initBtns() {
    this.elBtnClose1.addEventListener("click", this.onClickClose.bind(this));
    this.elBtnClose2.addEventListener("click", this.onClickClose.bind(this));
    this.elBkg.addEventListener("click", this.onClickClose.bind(this));

    this.elBtnClub.addEventListener("click", this.onClickBtnClub.bind(this));
    this.elBtnValidateClub.addEventListener("click", this.onClickBtnValidateClub.bind(this));

    const items = [].slice.call(this.elShaving.querySelectorAll(".btn"));
    const el = this.elShaving.querySelector("p");

    items.forEach((item, i) => {
      item.addEventListener("click", event => {
        el.innerHTML = item.getAttribute("data-text");
        this.elShaving.querySelector(".active").classList.remove("active");
        item.classList.add("active");
        this.elBtnValidateClub.setAttribute("href", item.getAttribute("data-href"));
        this.sendStatsClubImpression();
      });
    });

    const btnsBuy = [].slice.call(this.elBtnsBuy);
    btnsBuy.forEach((btn, i) => {
      btn.addEventListener("click", event => {
        fbq("track", "AddToCart", {
          content_name: btn.getAttribute("data-label-stats"),
          content_category: "Heated Razor",
          value: btn.getAttribute("data-price-stats"),
          currency: document.body.dataset.currency
        });

        window.dataLayer.push({
          event: "eec.add",
          ecommerce: {
            currencyCode: document.body.dataset.currency,
            add: {
              actionField: {
                list: "Heated Razor Starter kit panel"
              },
              products: [
                {
                  id: btn.getAttribute("data-id-stats"),
                  name: btn.getAttribute("data-label-stats"),
                  price: btn.getAttribute("data-price-stats"),
                  brand: "Gillette Labs",
                  category: "Heated Razor",
                  //position: el.getAttribute("data-position-stats"),
                  variant: btn.getAttribute("data-variant-stats"),
                  quantity: 1 //should be dynamic
                }
              ]
            }
          }
        });

        window.dataLayer.push({
          event: "HRButtonClick",
          eventCategory: "button",
          eventAction: "click",
          eventLabel: btn.getAttribute("data-code-stats")
        });
      });
    });
  }

  showPopinClub() {
    this.elPopinClub.classList.add("show");
    this.sendStatsClubImpression();
  }

  showPopinBuy() {
    this.elPopinBuy.classList.add("show");
    this.sendStatsBuyImpression();
  }

  sendStatsBuyImpression() {
    window.dataLayer.push({
      event: "eec.impression",
      ecommerce: {
        currencyCode: document.body.dataset.currency,
        impressions: [
          {
            id: "81701160",
            name: "Heated Razor Starter Kit",
            //price: this.elBtnValidateClub.getAttribute("data-price-stats"),
            brand: "Gillette Labs",
            category: "Heated Razor",
            position: 1,
            variant: "Starter Kit_4",
            list: "Heated Razor Starter Kit Panel"
          }
        ]
      }
    });

    window.dataLayer.push({
      event: "eec.impression",
      ecommerce: {
        currencyCode: document.body.dataset.currency,
        impressions: [
          {
            id: "81701161",
            name: "Heated Razor Starter Kit",
            //price: this.elBtnValidateClub.getAttribute("data-price-stats"),
            brand: "Gillette Labs",
            category: "Heated Razor",
            position: 2,
            variant: "Starter Kit_8",
            list: "Heated Razor Starter Kit Panel"
          }
        ]
      }
    });

    window.dataLayer.push({
      event: "VirtualPageview",
      virtualPageURL: location.pathname + "/starter_kit_panel",
      virtualPageTitle: "Heated Razor – Starter Kit Panel"
    });
  }

  sendStatsClubImpression() {
    const el = this.elShaving.querySelector(".active");

    window.dataLayer.push({
      event: "eec.impression",
      ecommerce: {
        currencyCode: document.body.dataset.currency,
        impressions: [
          {
            id: "81700189",
            name: el.getAttribute("data-label-stats"),
            //price: this.elBtnValidateClub.getAttribute("data-price-stats"),
            brand: "Gillette Labs",
            category: "Heated Razor",
            position: el.getAttribute("data-position-stats"),
            variant: el.getAttribute("data-variant-stats"),
            list: "Heated Razor Subscription Panel"
          }
        ]
      }
    });

    window.dataLayer.push({
      event: "VirtualPageview",
      virtualPageURL: location.pathname + "/subscription_panel",
      virtualPageTitle: "Heated Razor – Subscription Panel"
    });
  }

  // -----------------------------------------------------o private handlers

  onClickClose() {
    this.el.classList.add("hidden");
    this.elPopinClub.classList.remove("show");
    this.elPopinBuy.classList.remove("show");
  }

  onClickBtnClub() {
    this.el.scrollTop = 0;
    this.showPopinClub();
    this.elPopinBuy.classList.remove("show");

    fbq("track", "ViewContent", {
      content_name: this.elBtnClub.getAttribute("data-label-stats"),
      content_category: "Heated Razor",
      value: this.elBtnClub.getAttribute("data-price-stats"),
      currency: document.body.dataset.currency
    });

    window.dataLayer.push({
      event: "HRButtonClick",
      eventCategory: "button",
      eventAction: "click",
      eventLabel: "discover_subscription"
    });
  }

  onClickBtnValidateClub() {
    fbq("track", "AddToCart", {
      content_name: this.elBtnValidateClub.getAttribute("data-label-stats"),
      content_category: "Heated Razor",
      value: this.elBtnValidateClub.getAttribute("data-price-stats"),
      currency: document.body.dataset.currency
    });

    const el = this.elShaving.querySelector(".active");

    window.dataLayer.push({
      event: "eec.add",
      ecommerce: {
        currencyCode: document.body.dataset.currency,
        add: {
          actionField: {
            list: "Heated Razor Subscription panel"
          },
          products: [
            {
              id: "81700189",
              name: this.elBtnValidateClub.getAttribute("data-label-stats"),
              price: this.elBtnValidateClub.getAttribute("data-price-stats"),
              brand: "Gillette Labs",
              category: "Heated Razor",
              //position: el.getAttribute("data-position-stats"),
              variant: el.getAttribute("data-variant-stats"),
              quantity: 1 //should be dynamic
            }
          ]
        }
      }
    });

    window.dataLayer.push({
      event: "HRButtonClick",
      eventCategory: "button",
      eventAction: "click",
      eventLabel: "add_to_cart_subscription"
    });
  }
}
