import Main from "./Main";
import RequestAnimationFrame from "./polyfills/RequestAnimationFrame";
import "./polyfills/picturefill";

//-----------------------------------------------------o
//-------------------------------o READY
//-----------------------------------------------------o
document.addEventListener("DOMContentLoaded", function() {
  let main = new Main();

  (function tick() {
    main.update();
    window.requestAnimationFrame(tick);
  })();
});
