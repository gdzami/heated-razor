const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PrettierPlugin = require("prettier-webpack-plugin");
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');
const I18nPlugin = require('i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const src = __dirname + '/../src/';
const public =__dirname + '/../public/'

const languages = {
  'fr-fr': require(src + "datas/fr-fr.json"),
  'de-ch': require(src + "datas/de-ch.json"),
  'fr-ch': require(src + "datas/fr-ch.json")
};


const lng = process.env.LNG || 'fr-fr';
const env = process.env.ENV || 'local';
const mode = process.env.MODE || 'prod';


let template; // 'fra' or 'other'
if (lng.endsWith('-fr')) {
  template = 'france';
} else if (lng.endsWith('-ch')) {
  template = 'suisse';
}

let root;
if (env == 'gillette') {
  if (lng == 'fr-fr') {
    root = '/rasoir-chauffant/';
  } else if (lng == 'fr-ch') {
    root = '/fr/rasoir-chauffant/';
  } else if (lng == 'de-ch') {
    root = '/de/heated-razor/';
  }

  if (mode == "preprod") {
    const keys = Object.keys(languages[lng]);
    for (const key of keys) {
      languages[lng][key] = languages[lng][key].replace('www.gillette.fr', 'gillette-fr.preprod-ziqy.co');
      languages[lng][key] = languages[lng][key].replace('club.gillette.ch', 'gillette-ch.preprod-ziqy.co');
    }
  }

} else if (env == 'fcinq') {
  root = '/gillette/heated-razor/';
} else {
  root = '/';
}


module.exports = {
   // context: src,
    entry: {
      app: [src+'js/Entry.js', src+'sass/styles.scss']
    },
    module: {
      rules: [
        { 
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.(scss|sass|css)$/i,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
             //   importLoaders: 2,
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: [
                  cssnano({ preset: 'default' }),
                  autoprefixer({
                    browsers: [
                      'last 3 versions', 
                      '> 3%',
                      'ie 10',
                      'ie 11'
                    ]
                  }),
        
                ],
                minimize: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                data: '$root: "' + root + '";',
                sourceMap: true
              }
            }
          ],
        }
      ]
    },
    resolve: {
      modules: [
        path.resolve(__dirname, 'node_modules')
      ]
    },
    output: {
      filename: 'dist/bundles/scripts.min.js',
      path: public,
      publicPath: ''
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'dist/bundles/styles-' + lng + '.min.css',
      }),
      new I18nPlugin(languages[lng]),
      new HtmlWebpackPlugin({
        filename: 'index-' + lng + (mode == "preprod" ? "-preprod" : "") + '.html',
        template: __dirname + '/../src/index-' + template + '.html'
      }),
      new PrettierPlugin({
        parser: "babel",
        printWidth: 140,               // Specify the length of line that the printer will wrap on.
        tabWidth: 2,                  // Specify the number of spaces per indentation-level.
        useTabs: false,               // Indent lines with tabs instead of spaces.
        semi: true,                   // Print semicolons at the ends of statements.
        encoding: 'utf-8',            // Which encoding scheme to use on files
        extensions: [ '.js' ]  // Which file extensions to process
      })
    ]
}